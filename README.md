### Description
This contains the code for a few bots I am running. You can find them at the following links:

* Prévertbot at https://botsin.space/@inventaireprevert
* Haïkubot at https://botsin.space/@basho
* Fédérabot at https://botsin.space/@cestquoilafede
* Éclipses at https://botsin.space/@eclipses
* Dico digital at https://botsin.space/@dicodigital
* Space game show in French at https://botsin.space/@quizdelespace

### Avatars
Credit for the avatars is:
    
* Prévertbot -> https://upload.wikimedia.org/wikipedia/commons/9/92/Jacques_Pr%C3%A9vert_signature.svg
* Haïkubot -> https://en.wikipedia.org/wiki/Matsuo_Bash%C5%8D#/media/File:Basho_by_Hokusai-small.jpg
* Fédérabot -> Icon by Nathanaël (@roipoussiere)
* Éclipses -> https://commons.wikimedia.org/w/index.php?curid=12397066
* Dico digital -> @maiwann


### Reusing
This repository is under a CC BY SA 4.0 licence. 

If you want to use these codes to run your own bots, first create an account on the instance of your choice. Then you will have to make a "keys.txt" file with the following template:

    url of instance
    email address
    name of account
    password
    mastodon user ID (can be set to `0` in most cases)

Use the botinit.py module in your bot.py script:

    from botinit.py import get_api
    api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")
    api.status_post("Toot text")

Run in a terminal:

    python bot.py <directory containing the keys.txt file>