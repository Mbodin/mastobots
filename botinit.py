from mastodon import Mastodon

def get_api(keyfile):
    with open(keyfile, 'r') as passwords:
        MASTODON_URL = passwords.readline()[:-1]
        MASTODON_EMAIL = passwords.readline()[:-1]
        MASTODON_USERNAME = passwords.readline()[:-1]
        MASTODON_PASSWORD = passwords.readline()[:-1]
        MASTODON_USER_ID = int(passwords.readline()[:-1])
    
    client_id, client_secret = Mastodon.create_app(
        MASTODON_USERNAME,
        scopes=['read', 'write'],
        api_base_url=MASTODON_URL
    )
    
    api = Mastodon(
        client_id,
        client_secret,
        api_base_url=MASTODON_URL
    )
    
    access_token = api.log_in(
        MASTODON_EMAIL,
        MASTODON_PASSWORD,
        scopes=["read", "write"]
    )
    
    api = Mastodon(
        client_id,
        client_secret,
        access_token,
        api_base_url=MASTODON_URL
    )

    return api, MASTODON_USER_ID
