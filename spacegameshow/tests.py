import gen
import os
import json


images = os.listdir('images')

def question_tests(language):
    print('testing data.json')
    questions, texts = gen.read_file(language)

    cnt = 0
    
    for q in questions:
        ok = True
        
        # does every question/explanation with an image have one attached?
        if q['question']['has_image']:
            try:
                im = q['question']['image']
            except:
                print("image", q)
                ok = False

        if q['explanation']['has_image']:
            try:
                im = q['explanation']['image']
            except:
                print("image", q)
                ok = False

        if im not in images:
            print("missing image", im)
            ok = False

        # do phrase type answers have a non-empy list of answers?
        if q['answer']['type']=='phrase':
            try:
                q['answer'][language][0]
            except:
                print(q['answer'])
                ok = False

        # do numerical type answers have a float answer?
        if q['answer']['type']=='numerical' or q['answer']['type']=='date':
            try:
                float(q['answer']['value'])
            except:
                print(q['answer'])
                ok = False

        # do duration type answers have a valid answer?
        if q['answer']['type']=='duration':
            match = re.match('([0-9]+[hms])*', q['answer']['value'])
            if not match:
                print(q['answer'])
                ok = False

        # check punctuation
        try:
            expl = q['explanation'][language]
        except:
            print(q)
            ok = False
        if len(q['explanation'][language]) > 2:
            try:
                assert(('.' in expl[-2:]) or
                       ('…' in expl[-2:]) or
                       ('!' in expl[-2:]))
            except:
                print("punctuation", expl)
                ok = False
        else:
            print("empty explanation", q)
            ok = False
        try:
            assert(('?' in q['question'][language][-2:]) or
                   ('…' in q['question'][language][-2:]) or
                   ('...' in q['question'][language][-4:]))
        except:
            print("punctuation", q['question'])
            ok = False

        # check toot length
        try:
            ans = q['answer'][language][0]
        except KeyError:
            ans = str(q['answer']['value'])
        if len(q['explanation'][language]) + len(ans) > 444:
            print(len(q['explanation'][language]) + len(ans), language, q['explanation'])
            ok = False
        if q['question']['en'] == "What is it?":
            assert(q['question']['has_image'])

        # Empty explanations
        if len(q['question'][language]) > 1:
            try:
                q['explanation'][language]
            except:
                print("explanation", q['question'][language])
                ok = False
        if ok:
            cnt += 1
            
    print(cnt, "out of" , len(questions), language, "ok")


def poll_tests(filename):
    print('testing', filename)
    
    with open(filename, 'r') as f:
        contents = json.load(f)
    all_polls = contents['polls']
    
    for i, poll in enumerate(all_polls):
        # Check images
        if poll['has_image']:
            try:
                im = poll['image']
            except Error:
                print(i, 'missing image')
            else:
                if im not in images:
                    print("missing image", im)

        # Do all polls have multiple choices?
        if len(poll['choices']) < 2:
            print(i, 'missing choices')
        if len(poll['choices']) > 4:
            print(i, 'too many choices')

        # Are the explanations short enough?
        assert(len(poll['answer']) < 500)
                
    print(i+1, 'polls ok')

    
    
if __name__ == "__main__":
    for lang in ['fr']:#, 'en']:
        question_tests(lang)
    print('----------------')
    #poll_tests('polls.json')
