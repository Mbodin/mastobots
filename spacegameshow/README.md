## Description

Ce dossier contient les fichiers pour le quiz de l'espace.

- `bot.py` : le script principal à lancer pour faire tourner le bot.
- `gen.py` : toutes les fonctions auxiliaires utilisées par `bot.py`.
- `data.json` : la base de données des questions dans laquelle pioche le bot.
- `tests.py` : script qui vérifie la validité du fichier `data.json`.

Des informations supplémentaires sur le bot peuvent être trouvées [sur mon blog](http://blog.isabelle-santos.space/blog/quizdelespace/). 

J'ai aussi implémenté des questions en mode « sondage », où le bot pose une question avec quelques choix de réponse, puis donne une explication au bout d'un certain temps.

- `polls.py` : le script à lancer pour faire tourner le bot en mode « sondage ».
- `polls.json` : la base de données de questions à choix multiples.


## Contribuer

Les contributions sur ce projet sont les bienvenues. 

### Corriger des erreurs

Vous pouvez directement proposer une PR sur le fichier [data.json](https://framagit.org/Moutmout/mastobots/-/blob/master/spacegameshow/data.json), ou bien m'informer de l'erreur sur [Mastodon](https://framapiaf.org/@Moutmout).

### Proposer des questions-réponses

Si vous êtes à l'aise avec les PR, vous pouvez ajouter vos questions au fichier [data.json](https://framagit.org/Moutmout/mastobots/-/blob/master/spacegameshow/data.json), en ajoutant à la liste un item au format :

```
,

    {
      "question": {
        "has_image": false, # Est-ce qu'une image est attachée à la question?
          "fr": "", # Le texte de la question en français
	  "en": "", # Le texte de la quesiton en anglais (optionel)
	  "image": "" # L'image si besoin
      },
      "answer": {
        "type": "phrase",
        "fr": [ # La liste de toutes les réponses acceptables. 
            "" # Le premier item dans la liste est celui qui sera indiqué dans la réponse.
        ],
	"en": [ # Idem en anglais (optionel)
	      ""
	  ]
      },
      "explanation": {
        "has_image": false, # Est-ce qu'une image est attachée à la réponse?
        "fr": "", # L'explication de la réponse en français
	"en": "" # Idem en anglais (optionel)
      }
    }
```

Sinon, vous pouvez m'envoyer vos suggestions de questions sur [Mastodon](https://framapiaf.org/@Moutmout).

### Proposer des traductions

À l'heure actuelle, seule une version française du bot tourne, mais pourquoi pas faire la même chose dans d'autres langues ?!

#### En anglais

Quelques questions-réponses ont déjà été traduites en anglais. Pour ajouter des traductions, vous pouvez ajouter du texte dans le fichier [data.json](https://framagit.org/Moutmout/mastobots/-/blob/master/spacegameshow/data.json) partout où il y a `"en"` (voir la section sur contribuer des questions-réponses).

#### Dans d'autres langues

Vous voulez voir le quiz dans d'autres langues ? Venez m'en parler sur [Mastodon](https://framapiaf.org/@Moutmout) ou faites un ticket [sur gitlab](https://framagit.org/Moutmout/mastobots/-/issues).

### Ajouter/améliorer des fonctionnalités

#### Vous savez coder et souhaitez participer de cette façon

Faites un ticket ou une PR :)

#### Vous ne savez pas ou ne voulez pas coder...

... mais vous avez une super idée d'amélioration ? Venez m'en parler sur [Mastodon](https://framapiaf.org/@Moutmout). 
