"""
Various functions used by the bot.

Author: Isabelle M. Santos, 2020
"""
import random
import json
import re
from time import sleep
from mastodon import MastodonNetworkError


numbers = {
    'fr': [
        'zéro', 'un', 'deux', 'trois', 'quatre',
        'cinq', 'six', 'sept', 'huit', 'neuf',
        'dix', 'onze', 'douze', 'treize', 'quatorze',
        'quinze', 'seize'],
    'en': [
        'zero', 'one', 'two', 'three', 'four',
        'five', 'six', 'seven', 'eight', 'nine',
        'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
        'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
        'twenty']
}


def read_file(language):
    """
    Get all the data from the `data.json` file.

    The first item in the data file is a list of strings. These strings are used
    to write out the answer toots.
    
    Parameters
    ----------
    language: string
        The 2-character language code.

    Returns
    -------
    The list with all the questions, answers and explanations.
    The default text for writing answer toots.
    """
    with open('data.json', 'r') as f:
        file_contents = json.load(f)
    return file_contents['questions'], file_contents[language]


def get_question_from_index(questions, idx):
    """
    Get the question with a given index from a list of questions.
    
    Print stuff out to terminal for debugging purposes, in case the bot crashes.

    Parameters
    ----------
    questions: list of dicts
        Each element in the list is one question+answer+explanation.
    idx: int
        The index of the question to be fetched from the list.

    Returns
    -------
    question: dict
    answer: dict
    explanation: dict
    """
    line = questions[idx]

    question = line['question']
    explanation = line['explanation']
    answer = line['answer']

    print(idx)
    print(question)
    print(answer)
    print(explanation)

    return question, answer, explanation


def get_random_question(questions):
    """
    Get a question with a random index from a list of questions.

    Parameters
    ----------
    questions: list of dicts
        Each element in the list is one question+answer+explanation.
    
    Returns
    -------
    question: dict
    answer: dict
    explanation: dict
    """
    idx = random.randint(0, len(questions) - 1)
    print(idx)

    return get_question_from_index(questions, idx)


def get_question(questions, indexes):
    """
    Get a question that hasn't been asked yet.

    Keep a list with the indexes of all the questions that haven't been asked
    yet. This list will be reset every time the bot is restarted, but at least
    it will prevent the same question from being asked multiple times in a row.

    Parameters
    ----------
    questions: list of dicts
        Data that was read from the `data.json` file.
    indexes: list of ints
        Indexes of questions that have not yet been asked.
    """
    idx = random.choice(indexes)
    indexes.remove(idx)

    return indexes, get_question_from_index(questions, idx)


def wait_to_post(api, *args, **kwargs):
    """
    Try posting until there is no connection error.

    Sometimes, the internet connection crashes. To prevent the bot from crashing
    when this happens, catch the network error and try again later.

    Parameters
    ----------
    api: Mastodon
        Connection to the Mastodon API.

    Returns
    -------
    post: 
        The toot that was posted to Mastodon.
    """
    ok = False
    while not ok:
        try:
            post = api.status_post(*args, **kwargs)
        except MastodonNetworkError:
            print("No connection")
            sleep(300)
        else:
            ok = True
    return post


def post_question(api, question, language):
    """
    Make a post with a question in it.

    Parameters
    ----------
    api: Mastodon
        Connection to the Mastodon API.
    question: dict
        One question from the `data.json` file.
    language: string
        2-character language code.

    Returns
    -------
    post: 
        The toot that was posted to Mastodon.
    """
    if question['has_image']:
        media = api.media_post('./images/' + question['image'])
        post = wait_to_post(
            api,
            question[language],
            media_ids=media.id,
            visibility='public')
    else:
        post = wait_to_post(
            api,
            question[language],
            visibility='public')
    return post


def parse_post(toot_content):
    """
    Read a toot and extract information from it.

    Parameters
    ----------
    toot_content: str
        The content of a reply.

    Returns
    -------
    The content of the reply from which HTML tags and Mastodon handles have been removed.
    """
    tokens = toot_content.replace('>', '<').split('<')
    tokens = [t for t in tokens
              if (t not in [
                      'p', '/p', '/a', '/span', 'span', 'br',
                      '@', '',
                      'quizdelespace']
                  and 'a href=' not in t
                  and 'span class=' not in t
                  and 'class="' not in t
                  and 'target="' not in t)]
    return ' '.join(tokens)


def extract_floats(string, language):
    """
    Get floats out of a string.

    When the answer to a question is a number, you want to check if replies
    contain any numbers and figure out what those numbers are.

    Usage
    -----
    print(extract_floats('hi 123'))
    print(extract_floats('hi12.3'))
    print(extract_floats('12,3hi'))
    print(extract_floats('1or2'))
    print(extract_floats('1, 2, 3))

    Parameters
    ----------
    string: str
        The string to find numbers in
    language: str
        2-character language code

    Returns
    -------
    list of strings
        All the numbers that were found in the initial string.
    """
    if language == 'en':
        res = re.findall("[+-]?\d+[\.,]?\d*", string)  # noqa: W605
    if language == 'fr':
        res = re.findall("[+-]?\d+[\., ]?\d*", string)  # noqa: W605
        res = [e.replace(",", ".") for e in res]
        res = [e.replace(" ", "") for e in res]
    return res


def check(toot_content, accepted, language):
    """
    Determine whether a toot contains the right answer.

    There currently are 4 types of answers: `phrase`, `numerical`, `date`, and `duration`.
    The test to determine whether `toot_content` contains the right answer
    depends on the type of answer.

    Parameters
    ----------
    toot_content: string
        The content of a reply
    accepted: dict
        The accepted answer that was read from `data.json`.
    language: str
        2-character language code.

    Returns
    -------
    bool
        True if the reply contains an accepted answer.
    """
    answer = parse_post(toot_content)
    ok = False
    if accepted['type'] == 'phrase':
        solutions = accepted[language]
        for a in solutions:
            if a.lower().replace(" ", "") in answer.lower().replace(" ", ""):
                ok = True

    if accepted['type'] == 'numerical':
        solution = float(accepted['value'])
        matches = extract_floats(answer, language)
        for val in matches:
            try:
                parsed_val = float(val)
            except ValueError:
                pass
            # A margin of error of 10% is accepted on the answer.
            if parsed_val < 1.1 * solution and parsed_val > .9 * solution:
                ok = True
        # Also check if the number was spelled out
        try:
            number = numbers[language][int(solution)]
        except IndexError:
            pass
        else:
            if number in answer:
                ok = True

    if accepted['type'] == 'date':
        solution = float(accepted['value'])
        regex = re.compile('\d+')  # noqa: W605
        for number in regex.findall(answer):
            try:
                parsed_val = float(number)
            except AttributeError:
                pass
            else:
                # A margin of error of 8 years on the answer.
                if parsed_val < solution + 9 and parsed_val > solution - 9:
                    ok = True

    if accepted['type'] == 'duration':
        solution = str(accepted['value'])
        hours = {'fr': "heure", 'en': "hour"}[language]
        minutes = {'fr': "hour", 'en': "minute"}[language]
        seconds = {'fr': "seconde", 'en': "second"}[language]
        name = {'h': hours, 'm': minutes, 's': seconds} # The name of each unit.
        multi = {'h': 3600, 'm': 60, 's': 1} # The time in seconds of each unit.
        # To check an answer, we check for all occurrence of a number followed by
        # a unit, possibly with spaces in between (examples: 3h, 3 hour(s)).
        # These values are then converted in seconds and added to the variable v.
        # Hence, an answer like "I think that it will be 2 hours and 3s." will
        # yield of a v of 3600 * 2 + 3 * 1.
        # Note that answers like "3h30" will only be parsed as 3 hours because
        # of the missing unit for minutes: there is room for improvement.
        v = 0
        accepted = 0
        for u in multi:
            regexp = re.compile('([0-9]+) *(' + u + '|' + name[u] + ')')
            for number in regexp.findall(solution):
                accepted += multi[u] * number[0]
            for number in regexp.findall(answer):
                try:
                    parsed_val = float(number[0])
                except ValueError:
                    pass
                v += multi[u] * parsed_val
        # At this stage, the variable v contains the parsed answer in seconds,
        # and the variable accepted contains the parsed solution in seconds.
        # A margin of error of half an hour is accepted on the answer.
        if v < accepted + 30 * 60 and v > accepted - 30 * 60:
            ok = True

    return ok


def get_answer(current_answers, language):
    """
    Read the answer from the data.

    In the answer toot, the bot posts the "most correct" answer. Get this answer
    from `data.json`.

    Parameters
    ----------
    current_answers: dict
        The answers dict that was read from `data.json`
    language: str
        2-character language code.

    Returns
    -------
    str
        The "most correct" answer.
    """
    if current_answers['type'] == 'phrase':
        answer = current_answers[language][0]
    if current_answers['type'] == 'numerical':
        if language == 'fr':
            answer = str(current_answers['value']).replace('.', ',')
        else:
            answer = str(current_answers['value'])
    if current_answers['type'] == 'date':
        answer = str(current_answers['value'])
    if current_answers['type'] == 'duration':
        hours = {'fr': "heure", 'en': "hour"}[language]
        minutes = {'fr': "hour", 'en': "minute"}[language]
        seconds = {'fr': "seconde", 'en': "second"}[language]
        plural = {'fr': 's', 'en': 's'}[language]
        solution = re.findall("[0-9]+[a-z]+", str(current_answers['value']))
        answer = ""
        for v in solution:
            (number, unit) = re.match('([0-9]+)([a-z]+)', v).groups()
            if answer != "":
                answer += ", "
            if number != 0:
                answer += number + ' ' + {'h': hours, 'm': minutes, 's': seconds}[unit]
            if number > 0:
                answer += plural

    return answer


def post_answer(api, post, answers, explanation, language, toots, found,
                ok_toot=None):
    """
    Make a toot with the answer to a question.

    All answer toots have the same structure: "Congrats to [...], the answer
    was [...]". The elements that are common to all answer toots are contained
    in `data.json`. 

    Parameters
    ----------
    api: Mastodon
    post: 
        The toot that asked the question.
    answers: dict
        The answers dict from `data.json`.
    explanation: dict
        The explanation dict from `data.json`.
    language: str
        2-character language code.
    toots: list of strings
        The text that strings the different parts of the answer+explanation together.
    found: bool
        Has the correct answer to the question been found?
    ok_toot: 
        The toot that contains the correct answer. Only needed if `found==True`.
    """
    answer = get_answer(answers, language)

    if found:
        toot_text = (toots[0] + str(ok_toot.account.acct) + toots[1] +
                     answer + ". " + explanation[language])
    else:
        toot_text = (toots[2] + answer + ". " + explanation[language])

    if explanation['has_image']:
        media = api.media_post('./images/' + explanation['image'])
        post = wait_to_post(
            api,
            toot_text,
            visibility='public',
            in_reply_to_id=post,
            media_ids=media.id
        )
    else:
        post = wait_to_post(
            api,
            toot_text,
            visibility='public',
            in_reply_to_id=post
        )

    sleep(10)
    return post


def get_replies(api, since_post):
    """
    Fetch reply toots that were made to a question.

    Parameters
    ----------
    api: Mastodon
    since_post:
        Message received by the bot. Only consider replies posterior to `since_post`.

    Returns
    -------
    list
        All the messages received after `since_post`.
    """
    ok = False
    while not ok:
        try:
            notifs = api.notifications(since_id=since_post)
        except MastodonNetworkError:
            sleep(300)
        else:
            ok = True
    replies = [e for e in notifs if e.type == 'mention']
    return replies


if __name__ == "__main__":
    questions, text = read_file('fr')
    q, a, e = get_random_question(questions)
    print(get_answer(a, 'fr'))
