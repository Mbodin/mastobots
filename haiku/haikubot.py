# coding=utf-8

from mastodon import Mastodon
from gen import poem
from time import sleep
import sys
from botinit import get_api

current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")

f1 = current_directory + "first.txt"
f2 = current_directory + "second.txt"
f3 = current_directory + "third.txt"

# Get things done
while True:
    api.status_post(
        poem(f1, f2, f3),
        visibility='public'
    )
    sleep(60000)
